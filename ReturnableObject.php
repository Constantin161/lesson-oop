<?php
namespace lesson9;

abstract class ReturnableObject
{
    
	protected $body = array();

    abstract public function getBodyContent();

    // Метод для очистки данных, введенных пользователем.
	protected function clearStr($arr){
		foreach ($arr as $key=>$value){
		$value=strip_tags($value);
		$value=stripslashes($value);
		$value=htmlspecialchars($value);
		$value=trim($value);
		$arr[$key] = $value;
		}
		return $arr;
	}
}


// Класс для json объектов
class ReturnablejsonObject extends ReturnableObject 
{
	public function setBodyContent($data) {
		if (!empty($data)){
			$body = json_decode($data, true);
			$body = $this->clearStr($body);
			$this->body = $body;
		}
	}

	public function getBodyContent(){
		$body = $this->body;
		$body = json_encode($body);
		echo $body;
	}
} 

// Класс для html объектов

class ReturnablehtmlObject extends ReturnableObject
{
	public function setBodyContent($data){
		if (!empty($data)) 
		{	// Если есть что-то в теле запроса
			// Парсим строку
			parse_str($data, $body);
			// Очищаем строку от ненужных символов, тегов и прочего
			$body = $this->clearStr($body);
			$this->body = $body;			
		}
	}

	public function getBodyContent(){
		$body = $this->body;
		extract($body);
		include('form.html');
	}

	public function daTak(){
		$file = file_get_contents("1.json");
		$body = json_decode($file, true);
		return $body;
	}

	/*public function getBodyContent() {
		
		{
			extract($this->body);//$request->getBody());
			include($this->page);
		}
	}*/
}