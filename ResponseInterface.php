<?php
namespace lesson9;

interface ResponseInterface
{

    public function getHeader();

    public function setHeader();

    public function getHeaders();

    public function getBody();

    public function setBody(ReturnableObject $object);
}
