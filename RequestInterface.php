<?php
namespace lesson9;

interface RequestInterface
{
    public function getRequestTarget();

    public function getMethod();

    public function getHeader();

    public function getHeaders();

    public function getBody();
}
