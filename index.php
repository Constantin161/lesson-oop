<?php
use lesson9\Application;
use lesson9\Request;
use lesson9\Response;
use lesson9\ReturnablehtmlObject;
use lesson9\ReturnablejsonObject;

mb_internal_encoding('UTF-8');

error_reporting(E_ALL);

foreach (glob(__DIR__ . DIRECTORY_SEPARATOR . '*.php') as $file) {
    if (is_readable($file) && is_file($file)) {
        require_once $file;
    }
}

$app = new Application;
$request = new Request;
$response = new Response;
$returnableObject = $app->setContentType($request, $response);
$app->execute($request, $response, $returnableObject);
