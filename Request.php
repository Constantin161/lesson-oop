<?php
namespace lesson9;

/**
*
*/
class Request 
{
	protected $target;

	protected $headers;

	protected $method;

	protected $body = array();

	public function __construct()
	{
		mb_internal_encoding("utf-8");
		//$headers = getallheaders(); // На сервере Нетологии не работает эта функция, подозреваю из-за сервера nginx
		//Поэтому использую такую штуку и получаем заголовки запроса:
		foreach($_SERVER as $key=>$header){
			if(mb_strpos($key, "HTTP_") !== false)
			{
					$key = str_replace(array("HTTP_" , "_"), array("" , "-"), $key);
					$key = mb_convert_case($key, MB_CASE_TITLE);
					$this->headers[$key] = $header;
			}
		}

		// Получаем цель
		$this->target = $_SERVER['REQUEST_URI'];

		// Получаем метод запроса
		$this->method = $_SERVER['REQUEST_METHOD'];

		// Получаем тело запроса
		$stdin = fopen('php://input', 'r');
		$this->body = stream_get_contents($stdin);
		
	}

	public function getRequestTarget()
	{
		return $this->target;
	}

	public function getMethod()
	{
		return $this->method;
	}

	public function getHeader($header)
	{
		if (isset($this->headers[$header]))
		{
			return $this->headers[$header];
		}
		return null; 
	}

	public function getHeaders()
	{
		return $this->headers;
	}

	public function getBody()
	{
		
		return $this->body;
	}
}