<?php
namespace lesson9;

interface RenderableInterface
{
    public function render();
}
