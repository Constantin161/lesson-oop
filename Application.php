<?php
namespace lesson9;

class Application
{
    /**
     * Метод, показывающий какое минимальное количество методов у вас должны быть реализовано в объекте Request
     * @param Request $request
     */
    public function checkRequest(Request $request)
    {
        $requestTarget = $request->getRequestTarget();
        $method        = $request->getMethod();
        $header        = $request->getHeader();
        $headers       = $request->getHeaders();
        $body          = $request->getBody();
    }

    /**
     * Метод, показывающий какое минимальное количество методов у вас должны быть реализовано в объекте Response
     * @param Response $request
     */
    public function checkResponse(Response $request)
    {
        $header        = $request->getHeader();
        $headers       = $request->getHeaders();
        $request->setBody('<html></html>');
        $request->setHeader('Content-Type', 'text/html');
    }

    public function execute(Request $request, Response $response, ReturnableObject $returnableObject)
    {
        $body = $request->getBody();
        $returnableObject->setBodyContent($body);
        $response->setBody($returnableObject);
    }

    public function setContentType(Request $request, Response $response){
        if (mb_strpos($request->getheader('Accept'), 'text/html') !== false) {
            $response->setHeader('Content-Type', 'text/html');
            $returnableObject = new ReturnablehtmlObject;
        }
        elseif (mb_strpos($request->getheader('Accept'), 'application/json') !== false) {
            $response->setHeader('Content-Type', 'aplication/json');
            $returnableObject = new ReturnablejsonObject;
        }
        return $returnableObject;
    }
}